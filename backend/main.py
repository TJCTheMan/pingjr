import serverPing
import discord

intents = discord.Intents.default()
intents.message_content = True

client = discord.Client(intents=intents)

@client.event
async def on_ready():
    print(f'We have logged in as {client.user}')

@client.event
async def on_message(message):
    if message.content.startswith('$server'):
        await message.channel.send(serverPing.serverping())
    if message.content.startswith('$list'):
        await message.channel.send(serverPing.playerlist())
if __name__ == '__main__':
    client.run('YOUR TOKEN HERE')
