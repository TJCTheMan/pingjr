from mcstatus import JavaServer
from time import sleep

serverOnline = False

#Define serverping function
def serverping():
    #setting IP and Port
    ip = "ruiotech.com"
    port = "25502"

    if port == "":
        port = "25565"

    if port == "*":
        server = JavaServer.lookup(ip)
    else:
        server = JavaServer.lookup(ip + ":" + port)



    status = server.status()    

    if (int(server.ping()) < 900):
        global serverOnline
        serverOnline = True
        return ("The server is ONLINE, and has " + str(status.players.online) + " players online.")
    else:
        return ("The server is offline")


#attempting to set a function that querys the server and returns a list of players.
def playerlist():
    #setting IP and Port
    ip = "ruiotech.com"
    #does this need to be the Query port??? 
    port = "25501"

    if port == "*":
        server = JavaServer.lookup(ip)
    else:
        server = JavaServer.lookup(ip + ":" + port)

    query = server.query()
    #If the server returns as online, try to query the player list, if not, spit an error.
    if (serverOnline == True):
        try:
            serverPlayerList = ("Players online:", ", ".join(query.players.names))
            #If the player list is not empty, e.g. if people are online, output the list of players. If the list is empty, say so.
            if serverPlayerList is not None:
                return (serverPlayerList)
            else:
                return ("The player list is empty.")
        except:
            return "Query could not be completed."
    else:
        return "Server is Offline."





'''
if port == "*":
    while True:
        status = server.status()
        query = server.query()
        ping = server.ping()
        print("---------- " + ip + ":" + f"{query.raw['hostport']}" + " ----------")
        print(f"Version: v{status.version.name} (protocol {status.version.protocol})")
        print(f'Description: "{status.description}"')

        print(f"Host: {query.raw['hostip']}:{query.raw['hostport']}")
        print(f"Software: v{query.software.version} {query.software.brand}")
        print(f"Plugins: {query.software.plugins}")
        print(f"Ping: {ping} ms")
        print(f"Players: {status.players.online}/{status.players.max}")
        print(f"Players Online: {query.players.names}")
        print("\n")
        sleep(delay_int)

if port != "*":
    while True:
        status = server.status()
        query = server.query()
        ping = server.ping()
        print("---------- " + ip + ":" + port + " ----------")
        print(f"Version: v{status.version.name} (protocol {status.version.protocol})")
        print(f'Description: "{status.description}"')

        print(f"Host: {query.raw['hostip']}:{query.raw['hostport']}")
        print(f"Software: v{query.software.version} {query.software.brand}")
        print(f"Plugins: {query.software.plugins}")
        print(f"Ping: {ping} ms")
        print(f"Players: {status.players.online}/{status.players.max}")
        print(f"Players Online: {query.players.names}")
        print("\n")
        sleep(delay_int)
        '''